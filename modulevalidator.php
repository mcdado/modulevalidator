<?php

class ModuleValidator extends Module
{

    public function __construct()
    {
        $this->author = 'PrestaShop';
        $this->bootstrap = true;
        $this->name = 'modulevalidator';
        $this->tab = 'others';
        $this->version = '0.1.0';

        $this->displayName = $this->l('Module validator');
        $this->description = $this->l('Validate your PrestaShop module during your developments.');

        parent::__construct();
    }

    public function install()
    {
        return parent::install() &&
        $this->installAdminTab() &&
        $this->registerHook('backOfficeHeader') &&
        $this->registerHook('backOfficeTop');
    }

    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(

                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-key"></i>',
                        'desc' => $this->l('Enter a valid API key'),
                        'name' => 'MODULE_VALIDATOR_API_KEY',
                        'label' => $this->l('API key'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigFormValues()
    {
        return array(
            'MODULE_VALIDATOR_API_KEY' => Configuration::get('MODULE_VALIDATOR_API_KEY', null),
        );
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitModuleValidatorModule')) {
            $this->_postProcess();
        }

        return $this->renderForm();
    }

    protected function _postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') && (Tools::getValue('configure') != $this->name)) {
            $this->context->controller->addCSS($this->_path . 'views/css/modulevalidator.css');
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path . 'views/js/modulevalidator.js');
            $this->context->smarty->assign('validator_api_key', Configuration::get('MODULE_VALIDATOR_API_KEY', null));
        }
    }

    public function hookBackOfficeTop()
    {
        if (Tools::getValue('configure') && (Tools::getValue('configure') != $this->name)) {
            $this->context->smarty->assign(array(
                'module_name' => Tools::getValue('configure'),
                'module_validator_link' => $this->context->link->getAdminLink('AdminModuleValidator'),
            ));

            return $this->display($this->local_path, 'views/templates/admin/toolbar.tpl');
        }
    }

    public function installAdminTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminModuleValidator';
        $tab->name = array();

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Module validator';
        }

        $tab->id_parent = (int)Tab::getIdFromClassName('AdminAdmin');
        $tab->module = $this->name;

        return $tab->add();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModuleValidatorModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

}
