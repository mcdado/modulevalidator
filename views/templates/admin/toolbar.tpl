<div class="panel" id="module-validator">
	<div class="panel-heading">
		<i class="icon icon-cogs"></i> Modules Validation Toolbar
	</div>
	<div class="panel-body">
		{* Actions *}
		<div class="module-validator-request btn-group pull-left" role="group">
			<form method="post" target="_blank" action="{$module_validator_link|escape:'html':'UTF-8'}&amp;module={$module_name|escape:'htmlall':'UTF-8'}">
				<button type="submit" class="btn btn-primary">
					Validate this module
				</button>
			</form>
		</div>

		{* Errors *}
		<div class="module-validator-report pull-left">
			<span class="">Results:</span>
			<span class="label label-warning validator-warnings">0</span>
			<span class="label label-danger  validator-errors">0</span>
		</div>

		<div class="clearfix"></div>

		{* Results *}
		<div class="module-validator-results">
			<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#module-validator-result" aria-expanded="false" aria-controls="module-validator-result">
				Open Results
			</button>
			<div class="collapse well" id="module-validator-result">
				<pre></pre>
			</div>
		</div>
	</div>
</div>
{addJsDef validator_api_key=$validator_api_key}
