
<div class="panel">
	<h2>
		{l s='Validation' mod='modulevalidator'}
	</h2>
	<p>
		{$validation|escape:'htmlall':'UTF-8'}
	</p>
</div>