/* global validator_api_key */

$(document).ready(function() {

    /*
    * Move the element in the DOM and animate its appearing in place.
    * This is needed because there is no proper hook.
    */
    var $validatorPanel = $('#module-validator').detach();
    $validatorPanel.prependTo('#content').delay('slow').slideDown();

    /*
    * Prefer an AJAX based request.
    */
    $('.module-validator-request form').submit(function (e) {
        e.preventDefault();
        var $this = $(this);

        $this.prop('disabled', 'disabled');

        $.ajax({
            type: $this.prop('method'),
            url: $this.prop('action') + '&ajax=1',
            beforeSend: function() {
                $('.module-validator-report').before('<div class="module-validator-loading btn-group"><i class="icon-refresh icon-spin icon-fw"></i></div>');
            },
            success: function(data, textStatus, xhr) {
                $this.removeProp('disabled');
                $('.module-validator-loading').remove();
                $('.module-validator-report').show();
                $('.module-validator-results').show();
                $('.module-validator-results pre').text(JSON.stringify(data, undefined, 2));
                if (data.Details.results.warnings) {
                    $('.validator-warnings').text(data.Details.results.warnings.toString());
                }
                if (data.Details.results.errors) {
                    $('.validator-errors').text(data.Details.results.errors.toString());
                }
                console.log(data);
            },
        });
    });
    
});
