<?php

class AdminModuleValidatorController extends ModuleAdminController
{
	public function __construct()
	{
		$this->bootstrap = true;
		$this->lang = false;
		$this->display = 'view';

		parent::__construct();
	}

	public function test()
	{
		$target_module_name = Tools::getValue('module');
		if ($target_module_name == $this->module->name) {
			return false;
		}

		$target_module_path = _PS_MODULE_DIR_ . $target_module_name;

		$archive_filename = uniqid($target_module_name . date('_Ymd_His_')) . '.zip';
		$archive_filepath = _PS_MODULE_DIR_ . $this->module->name . '/archives/' . $archive_filename;

		if ((file_exists($target_module_path) == false) || (is_dir($target_module_path) == false)) {
			return false;
		}

		$this->zipModule($target_module_path, $archive_filepath);
		$this->validation = $this->validateModule($archive_filepath);
	}

	public function renderView()
	{
		$this->test();
		
		if (Tools::getIsset('ajax')) {
			header('Content-Type: application/json');
			return $this->validation;	
		}

		$this->context->smarty->assign('validation', $this->validation);

		return parent::renderView();
	}

	protected function validateModule($archive_filepath)
	{
		$endpoint = "https://validator.prestashop.com/api/modules";
		$postfields = array(
			'key' => Configuration::get('MODULE_VALIDATOR_API_KEY', null),
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_POST, true);
		
		if (function_exists('curl_file_create')) {
			curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
			$postfields['archive'] = curl_file_create($archive_filepath, 'application/zip', basename($archive_filepath));
		} else {
			$postfields['archive'] = '@'.$archive_filepath.';type=application/zip';
		}

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		$response = curl_exec($ch);
		curl_close($ch);

		if ($response !== false) {
			return $response;
		}

		return '';
	}

	protected function zipModule($source, $destination)
	{
		if (extension_loaded('zip') == false || file_exists($source) == false) {
			return false;
		}

		$zip = new ZipArchive();

		if ($zip->open($destination, ZIPARCHIVE::CREATE) == false) {
			return false;
		}

		$source = str_replace('\\', '/', realpath($source));

		if (is_dir($source) === true) {
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source),
				RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file) {
				$file = str_replace('\\', '/', $file);

				if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) {
					continue;
				}

				$file = realpath($file);

				if (is_dir($file) === true) {
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				} elseif (is_file($file) === true) {
					$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
				}
			}
		} elseif (is_file($source) === true) {
			$zip->addFromString(basename($source), file_get_contents($source));
		}

		return $zip->close();
	}

}
